package ir.ali.mvvm_sample.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ali.mvvm_sample.R
import ir.ali.mvvm_sample.ui.base.MotherActivity

class MainActivity : MotherActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}